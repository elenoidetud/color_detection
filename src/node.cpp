#include "ros/ros.h"
#include "std_msgs/ColorRGBA.h"
#include "sensor_msgs/CompressedImage.h"
#include "opencv/cv.h"
#include <sstream>
#include <image_transport/image_transport.h>
#include <cv_bridge/cv_bridge.h>
#include <sensor_msgs/image_encodings.h>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/highgui/highgui.hpp>
#include "aura_msgs/TorsoCoord.h"
#include "aura_msgs/SkeletonColor.h"

// Nur für Tests
int x, y, torso_id;

// Beispielhaft
double xRange = 4.0;
double yRange = 3.0; // muss noch ausgemessen werden!!!

ros::Publisher* color_id_pub = 0;

void chatterCallback(const sensor_msgs::ImageConstPtr &msg)
{
  // Share if possible, returning a const CvImage
  cv_bridge::CvImageConstPtr cvSharedImage = cv_bridge::toCvShare(msg, "");
  int b = cvSharedImage->image.at<cv::Vec3b>(x, y)[0];
  int g = cvSharedImage->image.at<cv::Vec3b>(x, y)[1];
  int r = cvSharedImage->image.at<cv::Vec3b>(x, y)[2];
  ROS_INFO("b = %d, g = %d, r = %d", b, g, r);
  std_msgs::ColorRGBA colorMsg;
  colorMsg.r = r;
  colorMsg.g = g;
  colorMsg.b = b;
  aura_msgs::SkeletonColor skelColor;
  skelColor.color = colorMsg;
  skelColor.id = torso_id;
  color_id_pub->publish(skelColor);
}

void updatePosition(const aura_msgs::TorsoCoord &msg)
{
  double xFactor = 640 / xRange;
  double yFactor = 480 / yRange;

  x = msg.x * xFactor + 330;
  y = msg.y * yFactor + 240;
  torso_id = msg.id;
}

int main(int argc, char **argv)
{
  /**
   * The ros::init() function needs to see argc and argv so that it can perform
   * any ROS arguments and name remapping that were provided at the command line.
   * For programmatic remappings you can use a different version of init() which takes
   * remappings directly, but for most command-line programs, passing argc and argv is
   * the easiest way to do it.  The third argument to init() is the name of the node.
   *
   * You must call one of the versions of ros::init() before using any other
   * part of the ROS system.
   */
  ros::init(argc, argv, "color_detection");

  /**
   * NodeHandle is the main access point to communications with the ROS system.
   * The first NodeHandle constructed will fully initialize this node, and the last
   * NodeHandle destructed will close down the node.
   */
  ros::NodeHandle n;

  /**
   * The advertise() function is how you tell ROS that you want to
   * publish on a given topic name. This invokes a call to the ROS
   * master node, which keeps a registry of who is publishing and who
   * is subscribing. After this advertise() call is made, the master
   * node will notify anyone who is trying to subscribe to this topic name,
   * and they will in turn negotiate a peer-to-peer connection with this
   * node.  advertise() returns a Publisher object which allows you to
   * publish messages on that topic through a call to publish().  Once
   * all copies of the returned Publisher object are destroyed, the topic
   * will be automatically unadvertised.
   *
   * The second parameter to advertise() is the size of the message queue
   * used for publishing messages.  If messages are published more quickly
   * than we can send them, the number here specifies how many messages to
   * buffer up before throwing some away.
   */
  ros::Publisher color_id_pub_main = n.advertise<aura_msgs::SkeletonColor>("skeletoncolor", 10);
  color_id_pub = &color_id_pub_main;

  ros::Subscriber sub = n.subscribe("/usb_cam/image_raw", 100, chatterCallback);
  ros::Subscriber torso_sub = n.subscribe("torsocoord", 100, updatePosition);

  ros::Rate loop_rate(10);

  /**
   * A count of how many messages we have sent. This is used to create
   * a unique string for each message.
   */
  int count = 0;
  while (ros::ok())
  {
    /**
     * This is a message object. You stuff it with data, and then publish it.
     */
    /*std_msgs::String msg;

    std::stringstream ss;
    ss << "hello world " << count;
    msg.data = ss.str();

    ROS_INFO("%s", msg.data.c_str());
    */

    /**
     * The publish() function is how you send messages. The parameter
     * is the message object. The type of this object must agree with the type
     * given as a template parameter to the advertise<>() call, as was done
     * in the constructor above.
     */
    //chatter_pub.publish(msg);

    ros::spinOnce();

    loop_rate.sleep();
    ++count;
  }

  return 0;
}