#!/bin/bash

MEDIAINS=`cat /var/log/installer/media-info`
IFS=' ' read -ra UBUNTUALL <<< "$MEDIAINS"
UBUNTUFLAVOR=${UBUNTUALL[0]}

if [ $UBUNTUFLAVOR == 'Ubuntu' ]
then
TRMINL="xterm"
elif [ $UBUNTUFLAVOR == 'Xubuntu' ]
then
TRMINL="xfce4-terminal"
else
echo "Dont know which terminal your distro uses... please fix launch script"
fi

echo "$UBUNTUFLAVOR: using $TRMINL >> own ip $ROS_IP >> master >> $ROS_MASTER_URI"
$TRMINL -e "bash -c \"export ROS_MASTER_URI=$ROS_MASTER_URI; export ROS_IP=$ROS_IP;source ~/catkin_ws/devel/setup.bash; roslaunch color_detection gdb.launch\"" &

sleep 4